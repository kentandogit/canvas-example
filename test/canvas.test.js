/**
 * @jest-environment jsdom
 */
let ele = document.createElement("canvas");
document.querySelector = jest.fn().mockReturnValue(ele);
const CANVASM = require('../js/canvas');


describe('When doing addTag', () => {
  let db = {collection:jest.fn().mockReturnValue(
        {doc:jest.fn().mockReturnValue({update:jest.fn()})}
    )};
  CANVASM.initDb(db);
  let spy = jest.spyOn(CANVASM.store,"push");

  it(`It creates a tag with 1 width and height and tag string = tag`, () => {
    CANVASM.addTag(1,1,1,1,"tag",true);
    expect(spy).toHaveBeenCalledWith({"x": 1,"y": 1,"w": 1, "h": 1, "tag":"tag"});
  });
  it(`It updates an existing tag string to = updated-tag`, () => {
    CANVASM.setActiveDraggingIndex(0);
    CANVASM.updateTag(1,1,1,1,"updated-tag");
    expect(CANVASM.store[0].tag).toBe("updated-tag")
  });
  it(`It removes an existing tag`, () => {

    let db = {collection:jest.fn().mockReturnValue(
          {doc:jest.fn().mockReturnValue(
              {update:jest.fn().mockReturnValue({then:jest.fn()})}
            )}
      )};
    CANVASM.initDb(db);
    let c = {drawImage: jest.fn()};
    CANVASM.setContext(c);
    CANVASM.removeTag(0);
    expect(CANVASM.store.length).toBe(0)
  });
  it(`It does not create a new tag with tag string = not-tag`, () => {
    CANVASM.addTag(1,1,1,1,"not-tag",false);
    expect(spy).not.toBeCalledWith({"x": 1,"y": 1,"w": 1, "h": 1, "tag":"not-tag"});
  });
});