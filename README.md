## Project URL: 
http://54.95.217.73/canvas-exercise/

## Running Unit test
1. Download or clone the project
2. CD to the project folder
3. Run `npm install`
4. Run `npm test`
5. ![img.png](img.png)

## Limitations
1. Localstorage/Sessionstorage can only store the data upto 3-5mb, only string data can be stored, so JSON.parse and JSON.string should be used all through out when using data objects, which I am having trouble saving the dataURL/Base64 image
2. Internet explorer 11 and less it is not working

## Project Issues (Hotfix)
1. Code is messy, not extendable and redudant codes
2. When draw beyond the canvas and then put the drawing inside afterwards, won't save.

## Future Features
1. Add button to remove all photos/images
2. Add button to clear all tags
3. When deleting image should not refresh the page
4. Clean code and refactor to make it more OOP and testable

## Resources
### IndexDB
* https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Using_IndexedDB
* http://jsfiddle.net/unclelongmao/VrS32/
* https://github.com/dannyconnell/localbase
* https://www.youtube.com/watch?v=KJnupY2HPCg

### Canvas 
* https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API
* https://devhints.io/canvas
* https://www.youtube.com/watch?v=gm1QtePAYTM
* https://sapandiwakar.in/using-html5-canvas-element-to-create-cool-tagging-interface/
* https://stackoverflow.com/questions/74376189/how-to-show-a-rectangle-while-drawing-it-on-mousemove-html-canvas

### Test Framework
* https://jestjs.io/


### Github Actions
* https://docs.github.com/en/actions/guides/building-and-testing-nodejs
* https://docs.github.com/en/github/administering-a-repository/defining-the-mergeability-of-pull-requests/managing-a-branch-protection-rule

### Docs
* https://jsdoc.app/index.html
