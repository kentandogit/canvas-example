/** @global */
const db = new Localbase('DBCanvas'); // indexDB Name

const elementThumbnails = document.querySelector("#thumbnails");  // element name for list of photos
let images = []; // local state of images (store images temporarily)
let selectedImageIndex = 0; // local state for selected images (get image index)

/**
 * Fetch the data from the indexDB.
 *
 * @function fetchData
 * @return a bunch of renders (renderImageToThumbnails, updateCanvasDataAndIndex & showButtons)
 */
db.collection('photos').get({ keys: true }).then(items => {
  if(items.length > 0) {
    items.map((item, index) => {
      images.push({ 
        key: item.key, 
        data: item.data 
      });
      renderImageToThumbnails(item.data.file, item.key, index);
    });

    updateCanvasDataAndIndex(selectedImageIndex);
    showButtons();
  }
});